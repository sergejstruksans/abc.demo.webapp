﻿using Microsoft.AspNetCore.Mvc;

namespace ABC.Demo.Web.api
{
    [ApiController]
    [Route("[controller]")]
    public class ApiControllerBase : ControllerBase
    {
        public ApiControllerBase()
        {
        }
    }
}